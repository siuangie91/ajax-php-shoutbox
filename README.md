# Ajax/PHP Shoutbox #

Project in Ajax and PHP. Uses Ajax to send POST data to database and pull out the data and show it in the shout box. From Eduonix's [**Projects In JavaScript & JQuery course**](https://stackskills.com/courses/projects-in-javascript-jquery) on **Stackskills**.

![Screen Shot 2015-12-28 at 10.39.04 PM.png](https://bitbucket.org/repo/R6M4yn/images/1599027499-Screen%20Shot%202015-12-28%20at%2010.39.04%20PM.png)